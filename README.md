Resume site for Nathan Petersen.

Site can be access at [resume.n8pete.com](https://resume.n8pete.com), hosted using GitLab Pages on a custom subdomain.

Visit my homepage at [n8pete.com](https://n8pete.com)

The site is designed to be universally accessible on Desktop or Mobile, with a print format to replicate a traditional resume.

The site is built using HTML with Tailwind CSS, and Font Awesome for the icons.

This site uses the [Universal Resume](https://github.com/WebPraktikos/universal-resume) project on GitHub, with inspiration from [Torey Littlefield's adaptation](https://github.com/toreylittlefield/my-custom-tailwind-resume). Much of it was customized to my own liking and relevance, and the packages were updated to their latest versions.

---
`npm run serve` for dev environment  
`npm run build` for production builds  

---

## Build Your Own Resume
Made With Universal Résumé Template  
Credit to [@webpraktikos on GitHub](https://github.com/WebPraktikos) for the original repo.  
Credit to [@toreylittlefield on GitHub](https://github.com/toreylittlefield) for some customization inspiration.  