function checkMode() {
    if (localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
        setDark()
      } else {
        setLight()
      }
}

function toggleMode() {
    if (localStorage.theme === "light") {
        setDark()
    } else {
        setLight();
    }
}

function setLight() {
    localStorage.theme = 'light'
    document.documentElement.classList.remove('dark')
    document.getElementById("dark_mode_symbol").classList = "text-white fa-regular fa-moon text-2xl"
    checkMode()
}

function setDark() {
    localStorage.theme = 'dark'
    document.documentElement.classList.add('dark')
    document.getElementById("dark_mode_symbol").classList = "text-zinc-800 fa-regular fa-sun text-2xl"
    checkMode()
}
